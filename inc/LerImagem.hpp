#ifndef LERIMAGEM_HPP
#define LERIMAGEM_HPP

using namespace std;

#include <string>

class LerImagem {
	private:
		string numero_magico;
		int tamanho;
		int posicao;
		int dimensao0;
		int dimensao1;
		string comentario;
		int cor;
		char * camada;
	public:
		LerImagem();
		void setNumeroMagico(string numero_magico);
		void setPosicao(int posicao);
		void setComentario(string comentario);
		void setDimensao0(int dimensao0);
		void setDimensao1(int dimensao1);
		void setCor(int cor);
		void setTamanho(int tamanho);
		void setCamada(char * camada, int tamanho);
		string getNumeroMagico();
		int getPosicao();
		string getComentario();
		int getDimensao0();
		int getDimensao1();
		int getCor();
		int getTamanho();
		char* getCamada();
};
#endif
