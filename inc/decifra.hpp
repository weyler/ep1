#ifndef DECIFRA_HPP
#define DECIFRA_HPP

#include "LerImagem.hpp"
#include <fstream>

using namespace std;

class decifra : public LerImagem{
	private:
		int escolha_do_filtro;
	public:
		decifra();
		void setFiltro(int escolha_do_filtro);
		int getFiltro();
};

#endif
