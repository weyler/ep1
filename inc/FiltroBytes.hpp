#ifndef FILTROBYTES_HPP
#define FILTROBYTES_HPP

#include "decifra.hpp"
#include "LerImagem.hpp"

class FiltroBytes : public decifra {
	private:
		int lsb;
		char letra;
	public:
		FiltroBytes();
		void AplicarFiltro(int posicao, int tamanho, int escolha_do_filtro, char * camada);
};
#endif
