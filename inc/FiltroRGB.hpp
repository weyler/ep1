#ifndef FILTRORGB_HPP
#define FILTRORGB_HPP

#include "decifra.hpp"
#include "LerImagem.hpp"

class FiltroRGB : public decifra {
	private:
		char * nova_imagem;
		fstream imagem_filtrada;
	public:
		FiltroRGB();
		void AplicarFiltro(int tamanho, int escolha_do_filtro, char * camada);
		char* getAplicarFiltro();
		void NovaImagem(char * nova_imagem, int dimensao0, int dimensao1, int cor, int escolha_do_filtro, int tamanho);
};

#endif
