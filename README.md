O programa compila e roda com os comandos basicos do Makefile:
make = compila o programa;
make run = roda o programa;

Menu requer apenas duas entradas:
nome do arquivo: entrada do nome da imagem. Ex: lena.pgm
filtro a ser utilizado: 1/2/3/4

No caso das imagens .ppm onde se deve criar um arquivo com a solução, tal
arquivo é criado na pasta raiz do projeto com o nome "ImagemFiltrada.ppm".

As imagens a serem dadas como entrada no programa devem estar na pasta raiz
do projeto.
