#include <fstream>
#include <iostream>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <array>
#include <cstdio>
#include "LerImagem.hpp"

using namespace std;
using std::stringstream;

LerImagem::LerImagem(){
}
void LerImagem::setNumeroMagico(string numero_magico){
	this->numero_magico = numero_magico;
}
void LerImagem::setPosicao(int posicao){
	this->posicao = posicao;
}
void LerImagem::setComentario(string comentario){
	this->comentario = comentario;
}
void LerImagem::setDimensao0(int dimensao0){
	this->dimensao0 = dimensao0;
}
void LerImagem::setDimensao1(int dimensao1){
	this->dimensao1 = dimensao1;
}
void LerImagem::setCor(int cor){
	this->cor = cor;
}
void LerImagem::setTamanho(int tamanho){
	this->tamanho = tamanho;
}
void LerImagem::setCamada(char * camada, int tamanho){
	this->camada = new char[tamanho];
	for (int i = 0; i < tamanho; i++){
		this->camada[i] = camada[i];
	}
}
string LerImagem::getNumeroMagico(){
	return numero_magico;
}
int LerImagem::getPosicao(){
	return posicao;
}
string LerImagem::getComentario(){
	return comentario;
}
int LerImagem::getDimensao0(){
	return dimensao0;
}
int LerImagem::getDimensao1(){
	return dimensao1;
}
int LerImagem::getCor(){
	return cor;
}
int LerImagem::getTamanho(){
	return tamanho;
}
char* LerImagem::getCamada(){
	return camada;
}
