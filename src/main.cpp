#include "LerImagem.hpp"
#include "decifra.hpp"
#include "FiltroBytes.hpp"
#include "FiltroRGB.hpp"

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

int main() {
	string nome_do_arquivo;
	string numero_magico;
	int tamanho;
	int dimensao0;
	int dimensao1;
	int escolha_do_filtro;
	int cor;
	int posicao;
	//int x;
	string comentario;
	string string_arquivo;
	fstream arquivo;

	cout << "Nome do arquivo: ";
	cin >> nome_do_arquivo;
	cout << endl;

	arquivo.open(nome_do_arquivo, fstream::in);
	if (arquivo == NULL) {
		cout << "Arquivo nao encontrado." << endl;
		exit(1);
	}
	arquivo.seekg(0, arquivo.end);
	tamanho = arquivo.tellg();
	arquivo.seekg(0, arquivo.beg);

	char * camada = new char[tamanho];

	cout << "(1)Filtro R | (2) Filtro G | (3) Filtro B | (4) Filtro Bytes" << endl;
	cout << "Escolha o filtro: ";
	cin >> escolha_do_filtro;
	cout << endl;
	while(escolha_do_filtro > 4 || escolha_do_filtro < 1){
		cout << "Filtro inválido." << endl << "Escolha novamente: ";
		cin >> escolha_do_filtro;
		cout << endl;
	}

	for (int i = 0; i < 5; i++){
		if (i == 1){
			if (escolha_do_filtro == 4){
				arquivo >> string_arquivo;
				arquivo >> string_arquivo;
				posicao = atoi(string_arquivo.c_str());
				getline(arquivo, string_arquivo);
				comentario = string_arquivo;
			} else {
				arquivo >> string_arquivo;
				posicao = atoi(string_arquivo.c_str());
				getline(arquivo, string_arquivo);
				comentario = string_arquivo;
			}
		}
		if(i != 1){
			arquivo >> string_arquivo;
		}
		if (i == 0){
			numero_magico = string_arquivo;
		}
		if (i == 2){
			dimensao0 = atoi(string_arquivo.c_str());
		}
		if (i == 3){
			dimensao1 = atoi(string_arquivo.c_str());
		}
		if (i == 4){
			//x = arquivo.tellg();
			cor = atoi(string_arquivo.c_str());
		}
	}
			for (int j = 0; j <  tamanho; j++) {
				arquivo.get(camada[j-1]);
			}
	//cout << x << endl;
	//for(int i = 0; i < 1000; i++){
	//	cout << camada[i];
	//}
	arquivo.close();
	//cout << endl;
	//cout << posicao << endl << dimensao0 << endl << dimensao1 << endl << cor << endl;

	LerImagem * Objeto1 = new LerImagem();
	decifra * Objeto2 = new decifra();
	FiltroRGB * Objeto3 = new FiltroRGB();
	FiltroBytes * Objeto4 = new FiltroBytes();
	Objeto1->setNumeroMagico(numero_magico);
	Objeto1->setPosicao(posicao);
	Objeto1->setComentario(comentario);
	Objeto1->setDimensao0(dimensao0);
	Objeto1->setDimensao1(dimensao1);
	Objeto1->setCor(cor);
	Objeto1->setTamanho(tamanho);
	Objeto1->setCamada(camada, tamanho);
	Objeto2->setFiltro(escolha_do_filtro);
	Objeto3->AplicarFiltro(Objeto1->getTamanho(), Objeto2->getFiltro(), Objeto1->getCamada());
	Objeto3->NovaImagem(Objeto3->getAplicarFiltro(), Objeto1->getDimensao0(), Objeto1->getDimensao1(), Objeto1->getCor(), Objeto2->getFiltro(), Objeto1->getTamanho());
	Objeto4->AplicarFiltro(Objeto1->getPosicao(), Objeto1->getTamanho(), Objeto2->getFiltro(), Objeto1->getCamada());

	delete(Objeto1);
	delete(Objeto2);
	delete(Objeto3);
	delete(Objeto4);
	delete[] camada;
}
